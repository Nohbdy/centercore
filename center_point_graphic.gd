extends TextureRect

@export var thing: PackedScene

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass


func _on_button_pressed() -> void:
	var instance = thing.instantiate()
	add_child(instance)
	set_editable_instance(instance, true)
	var countdownToTimerReset = Timer.new()
	countdownToTimerReset.one_shot = true
	countdownToTimerReset.wait_time = 300
	
